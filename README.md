# react-build-scripts

A custom version of [Facebook's react-scripts](https://github.com/facebook/create-react-app/tree/next/packages/react-scripts) package

Includes
* Split local development build
* SVG Sprites

## Usage with existing react project
`npm install react-build-scripts`

## Usage with create-react-app
`create-react-app <project-directory> --scripts-version react-build-scripts`
